<div id="<?php print $div_id; ?>"></div>
<script>
  window.addEventListener("load", function() {
    var foamtree = new CarrotSearchFoamTree({
      rainbowStartColor: "rgba(237, 159, 13, 1)",
      rainbowEndColor:   "rgba(237, 159, 13, 1)",
      id: "<?php print $div_id; ?>",
      onGroupDoubleClick: function (event) {
        var path = 'search?f[0]=field_informea_tags%3A' + event.group.id;
        window.location.href = path;
      },
    });

    function convert(clusters) {
      return clusters.map(function(cluster) {
        return {
          id:     cluster.id,
          label:  cluster.phrases.join(", "),
          weight: cluster.attributes && cluster.attributes["other-topics"] ? 0 : cluster.size,
          groups: cluster.clusters ? convert(cluster.clusters) : []
        }
      });
    };

    foamtree.set({
      dataObject: {
        groups: convert(Drupal.settings.foamtree.clusters)
      }
    });

  });

</script>